package org.ws.httphelper.spring;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.BeanUtils;
import org.ws.httphelper.builder.RequestConfigBuilder;
import org.ws.httphelper.core.pipeline.HttpHandler;
import org.ws.httphelper.model.field.ParamField;
import org.ws.httphelper.model.field.ParseField;
import org.ws.httphelper.model.template.RequestTemplate;
import org.ws.httphelper.spring.model.NameValue;
import org.ws.httphelper.spring.model.RequestProperties;
import org.ws.httphelper.template.DefaultClientConfig;
import org.ws.httphelper.template.DefaultHandlers;

public final class HttpHelperPropertiesBuilder {

    private HttpHelperProperties properties;

    private HttpHelperPropertiesBuilder(HttpHelperProperties properties) {
        this.properties = properties;
    }

    public static HttpHelperPropertiesBuilder builder(HttpHelperProperties properties){
        return new HttpHelperPropertiesBuilder(properties);
    }

    public RequestTemplate build() {
        RequestTemplate template = new RequestTemplate();
        if(properties.getClient() != null){
            template.setClientConfig(properties.getClient());
        }
        else {
            template.setClientConfig(DefaultClientConfig.okHttpClientConfig(4,3_000));
        }
        RequestProperties request = properties.getRequest();
        if(request != null){
            RequestConfigBuilder builder = RequestConfigBuilder.builder();
            builder.charset(request.getCharset())
                    .rootUrl(request.getRootUrl())
                    .method(request.getMethod())
                    .contentType(request.getContentType())
                    .responseType(request.getResponseType());
            if(request.isDefaultHandler()){
                builder.addHandler(DefaultHandlers.allHandlers());
            }
            Class<HttpHandler>[] handlers = request.getHandlers();
            if(ArrayUtils.isNotEmpty(handlers)){
                for (Class<HttpHandler> handlerClass : handlers) {
                    builder.addHandler(BeanUtils.instantiateClass(handlerClass));
                }
            }
            NameValue[] defaultHeaders = request.getDefaultHeaders();
            if(ArrayUtils.isNotEmpty(defaultHeaders)){
                for (NameValue defaultHeader : defaultHeaders) {
                    builder.addDefaultHeader(defaultHeader.getName(), defaultHeader.getValue());
                }
            }
            NameValue[] defaultCookers = request.getDefaultCookers();
            if(ArrayUtils.isNotEmpty(defaultCookers)){
                StringBuilder cookies = new StringBuilder();
                for (NameValue defaultCooker : defaultCookers) {
                    cookies.append(defaultCooker.getValue()).append(";");
                }
                builder.addDefaultHeader("Cookie",cookies.toString());
            }
            NameValue[] defaultParams = request.getDefaultParams();
            if(ArrayUtils.isNotEmpty(defaultParams)){
                for (NameValue defaultParam : defaultParams) {
                    builder.addDefaultParam(defaultParam.getName(),defaultParam.getValue());
                }
            }
            ParamField[] paramFields = request.getParamFields();
            if(ArrayUtils.isNotEmpty(paramFields)){
                for (ParamField paramField : paramFields) {
                    builder.addParamFields(paramField);
                }
            }
            ParseField[] parseHtmlFields = request.getParseHtmlFields();
            if(ArrayUtils.isNotEmpty(parseHtmlFields)){
                for (ParseField parseHtmlField : parseHtmlFields) {
                    builder.addParseHtmlFields(parseHtmlField);
                }
            }
            template.setRequestConfig(builder.build());
        }
        return template;
    }

}
