package org.ws.httphelper.support.pipeline;

import org.junit.Test;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ws.httphelper.core.pipeline.HandlerOrder;
import org.ws.httphelper.core.pipeline.RequestHandler;
import org.ws.httphelper.core.pipeline.ResponseHandler;
import org.ws.httphelper.exception.RequestException;
import org.ws.httphelper.exception.ResponseException;
import org.ws.httphelper.model.RequestContext;
import org.ws.httphelper.model.config.RequestConfig;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/** 
* DefaultHttpPipeline Tester. 
* 
* @author <Authors name> 
* @since <pre>十一月 14, 2020</pre> 
* @version 1.0 
*/ 
public class DefaultHttpPipelineTest {

    private static Logger log = LoggerFactory.getLogger(DefaultHttpPipelineTest.class.getName());

    DefaultHttpPipeline httpPipeline;
    ExecutorService executor;

    @Before
    public void before() throws Exception {
        httpPipeline = new DefaultHttpPipeline();
        executor = Executors.newFixedThreadPool(4);
    }

    /**
    *
    * Method: addFirst(HttpHandler handler)
    *
    */
    @Test
    public void testAddFirstHandler() throws Exception {
        httpPipeline.addFirst(new TestRequestHandler());
        httpPipeline.addFirst(executor,new TestRequestHandler2());
        httpPipeline.addFirst(new TestRequestHandler(),new TestRequestHandler2());
        httpPipeline.addFirst(executor,new TestResponseHandler(),new TestResponseHandler2());
    }

    /**
    *
    * Method: addLast(HttpHandler... handlers)
    *
    */
    @Test
    public void testAddLastHandlers() throws Exception {
        httpPipeline.addLast(new TestResponseHandler());
        httpPipeline.addLast(executor,new TestResponseHandler2());
        httpPipeline.addLast(new TestRequestHandler(),new TestRequestHandler2());
        httpPipeline.addLast(executor,new TestResponseHandler(),new TestResponseHandler2());
    }

    /**
    *
    * Method: remove(HttpHandler handler)
    *
    */
    @Test
    public void testRemove() throws Exception {
        httpPipeline.addFirst(new TestRequestHandler());
        httpPipeline.request(new RequestContext(null,null,new RequestConfig()));

        httpPipeline.remove(TestRequestHandler.class);
        httpPipeline.request(new RequestContext(null,null,new RequestConfig()));

        httpPipeline.remove(TestRequestHandler.class);
        httpPipeline.request(new RequestContext(null,null,new RequestConfig()));
    }

    /**
    *
    * Method: request(RequestContext requestContext)
    *
    */
    @Test
    public void testRequest() throws Exception {
        testAddFirstHandler();
        testAddLastHandlers();
        httpPipeline.request(new RequestContext(null,null,new RequestConfig()));
        httpPipeline.request(new RequestContext(null,null,new RequestConfig()));
        executor.awaitTermination(1, TimeUnit.SECONDS);
    }

    /**
    *
    * Method: response(RequestContext requestContext)
    *
    */
    @Test
    public void testResponse() throws Exception {
        testAddFirstHandler();
        testAddLastHandlers();
        httpPipeline.response(new RequestContext(null,null,new RequestConfig()));
        httpPipeline.response(new RequestContext(null,null,new RequestConfig()));
        executor.awaitTermination(1, TimeUnit.SECONDS);
    }

    @HandlerOrder(value = 10,level = HandlerOrder.OrderLevel.USER)
    static class TestRequestHandler implements RequestHandler {
        @Override
        public boolean handler(RequestContext requestContext) throws RequestException {
            log.info("call request TestResultHandler.");
            return true;
        }
    }

    @HandlerOrder(value = 10,level = HandlerOrder.OrderLevel.SYSTEM)
    static class TestRequestHandler2 implements RequestHandler {
        @Override
        public boolean handler(RequestContext requestContext) throws RequestException {
            log.info("call request TestResultHandler2.");
            return true;
        }
    }

    @HandlerOrder(value = 1,level = HandlerOrder.OrderLevel.USER)
    static class TestResponseHandler implements ResponseHandler {
        @Override
        public boolean handler(RequestContext requestContext) throws ResponseException {
            log.info("call request TestResponseHandler.");
            return true;
        }
    }

    @HandlerOrder(value = 1,level = HandlerOrder.OrderLevel.SYSTEM)
    static class TestResponseHandler2 implements ResponseHandler {
        @Override
        public boolean handler(RequestContext requestContext) throws ResponseException {
            log.info("call request TestResponseHandler2.");
            return true;
        }
    }
} 
