package org.ws.httphelper.builder;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ws.httphelper.HttpHelper;
import org.ws.httphelper.common.Constant;
import org.ws.httphelper.model.field.ExpressionType;
import org.ws.httphelper.model.field.ParamField;
import org.ws.httphelper.model.field.ParseField;
import org.ws.httphelper.model.field.ParseFieldType;
import org.ws.httphelper.model.http.ClientType;
import org.ws.httphelper.model.http.ContentType;
import org.ws.httphelper.model.http.HttpMethod;
import org.ws.httphelper.model.http.ResponseFuture;
import org.ws.httphelper.model.http.ResponseType;
import org.ws.httphelper.model.template.RequestTemplate;
import org.ws.httphelper.servlet.JettyServlet;
import org.ws.httphelper.support.annotation.TestRequestEntity;
import org.ws.httphelper.template.DefaultHandlers;

public class HttpHelperBuilderTest {

    private static Logger log = LoggerFactory.getLogger(HttpHelperBuilderTest.class.getName());

    private static JettyServlet jettyServlet = new JettyServlet(8888);

    private String rootUrl = "http://127.0.0.1:8888";

    @Before
    public void before() throws Exception {
        jettyServlet.start();
    }

    @Test
    public void testGetRequest(){
        RequestTemplate template = RequestTemplateBuilder.builder()
                .buildClientConfig()
                .privateClient(true)
                .followRedirects(true)
                .enableSsl(true)
                .threadNumber(4)
                .timeout(1_000)
                .clientType(ClientType.OK_HTTP_CLIENT)
                .requestConfig()
                .charset(Constant.UTF_8)
                .method(HttpMethod.GET)
                .contentType(ContentType.EMPTY)
                .responseType(ResponseType.HTML)
                .rootUrl(rootUrl)
                .addParamFields(new ParamField("key1","value1",true,"[\\d\\w]+"))
                .addParamFields(new ParamField("key2","value",true,"\\w+"))
                .addParamFields(new ParamField("key3","1",true,"\\d+"))
                .addHandler(DefaultHandlers.allHandlers())
                .addParseHtmlFields(new ParseField("title", ParseFieldType.STRING,"//title", ExpressionType.XPATH))
                .addParseHtmlFields(new ParseField("links", ParseFieldType.LIST,"a@href", ExpressionType.CSS))
                .addParseHtmlFields(new ParseField("date", ParseFieldType.STRING,"(\\d{4}(年|\\.)\\d{2}(月|\\.)\\d{2}日?)", ExpressionType.REGEX))
                .end()
                .build();

        HttpHelper httpHelper = HttpHelperBuilder.builder().builderByTemplate(template);

        TestRequestEntity requestEntity = new TestRequestEntity();
        requestEntity.setKey1("aaa");

        ResponseFuture future = httpHelper.request("/index.html",requestEntity);

        Assert.assertTrue(future.isSuccess());
        log.info(JSON.toJSONString(future, SerializerFeature.PrettyFormat));


    }
}
