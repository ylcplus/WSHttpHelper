package org.ws.httphelper.model.http;

public enum HttpMethod {
    GET, HEAD, POST, PUT, PATCH, DELETE;
}
