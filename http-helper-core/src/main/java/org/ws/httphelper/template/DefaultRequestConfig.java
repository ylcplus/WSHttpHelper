package org.ws.httphelper.template;

import org.ws.httphelper.builder.RequestConfigBuilder;
import org.ws.httphelper.common.Constant;
import org.ws.httphelper.model.config.RequestConfig;
import org.ws.httphelper.model.http.ContentType;
import org.ws.httphelper.model.http.HttpMethod;
import org.ws.httphelper.model.http.ResponseType;
import org.ws.httphelper.support.pipeline.handler.response.ParseHtmlHandler;
import org.ws.httphelper.support.pipeline.handler.response.ParseJsonHandler;
import org.ws.httphelper.support.pipeline.handler.response.ParseResponseTypeHandler;
import org.ws.httphelper.support.pipeline.handler.response.ParseXmlHandler;

/**
 * 默认的请求配置
 */
public class DefaultRequestConfig {

    /**
     * 默认Get方法html请求：适用于响应为html的网页请求
     * @return
     */
    public static RequestConfig getHtml(){
        return getHtml(null);
    }

    /**
     * 默认有rootUrl的Get方法html请求
     * @param rootUrl
     * @return
     */
    public static RequestConfig getHtml(String rootUrl){
        RequestConfigBuilder builder = RequestConfigBuilder.builder()
                // 设置rootUrl，在实际请求中使用相对路径path可自动拼接成绝对路径
                .rootUrl(rootUrl)
                // 编码：UTF-8
                .charset(Constant.UTF_8)
                // 请求方法：GET
                .method(HttpMethod.GET)
                // Content-Type: application/x-www-form-urlencoded
                .contentType(ContentType.X_WWW_FORM_URLENCODED)
                // 添加全部默认请求处理器
                .addHandler(DefaultHandlers.requestHandlers())
                // 添加响应类型解析处理器
                .addHandler(new ParseResponseTypeHandler())
                // 添加HTML解析处理器：若没有需要解析的数据，直接跳过
                .addHandler(new ParseHtmlHandler());
        return builder.build();
    }

    /**
     * 默认POST方法的json请求：适用于响应为JSON的REST接口
     * @return
     */
    public static RequestConfig postJson(){
        return postJson(null);
    }

    /**
     * 默认有rootUrl的POST方法的json请求
     * @param rootUrl
     * @return
     */
    public static RequestConfig postJson(String rootUrl){
        RequestConfigBuilder builder = RequestConfigBuilder.builder()
                // 设置rootUrl，在实际请求中使用相对路径path可自动拼接成绝对路径
                .rootUrl(rootUrl)
                // 默认编码：UTF-8
                .charset(Constant.UTF_8)
                // 请求方法：POST
                .method(HttpMethod.POST)
                // 请求参数类型：JSON
                .contentType(ContentType.RAW_JSON)
                // 响应类型：JSON
                .responseType(ResponseType.JSON)
                // Content-Type: application/json
                .addDefaultHeader(Constant.HEAD_CONTENT_TYPE,ContentType.RAW_JSON.getType())
                // 添加默认全部请求处理器
                .addHandler(DefaultHandlers.requestHandlers())
                // 添加解析响应类型处理器
                .addHandler(new ParseResponseTypeHandler())
                // 添加解析JSON处理器
                .addHandler(new ParseJsonHandler());
        return builder.build();
    }

    /**
     * 默认POST方法的xml请求：适用于响应为XML的接口请求
     * @return
     */
    public static RequestConfig postXml(){
        return postXml(null);
    }

    /**
     * 默认有rootUrl的POST方法的xml请求
     * @param rootUrl
     * @return
     */
    public static RequestConfig postXml(String rootUrl){
        RequestConfigBuilder builder = RequestConfigBuilder.builder()
                // 设置rootUrl，在实际请求中使用相对路径path可自动拼接成绝对路径
                .rootUrl(rootUrl)
                // 默认编码：UTF-8
                .charset(Constant.UTF_8)
                // 请求方法：POST
                .method(HttpMethod.POST)
                // 请求参数类型：XML
                .contentType(ContentType.RAW_XML)
                // 响应类型：XML
                .responseType(ResponseType.XML)
                // Content-Type: application/xml
                .addDefaultHeader(Constant.HEAD_CONTENT_TYPE,ContentType.RAW_XML.getType())
                // 添加默认全部请求处理器
                .addHandler(DefaultHandlers.requestHandlers())
                // 添加解析响应类型处理器
                .addHandler(new ParseResponseTypeHandler())
                // 添加解析XML处理器
                .addHandler(new ParseXmlHandler());
        return builder.build();
    }
}
