package org.ws.httphelper.builder.annotation;

import org.ws.httphelper.common.Constant;
import org.ws.httphelper.core.pipeline.HttpHandler;
import org.ws.httphelper.model.http.ContentType;
import org.ws.httphelper.model.http.HttpMethod;
import org.ws.httphelper.model.http.ResponseType;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@HttpRequest(method = HttpMethod.POST)
@Documented
public @interface Post {

    String value() default "";

    ContentType contentType() default ContentType.X_WWW_FORM_URLENCODED;

    ResponseType responseType() default ResponseType.HTML;

    RequestParam[] requestParams() default {};

    Class requestEntity() default Map.class;

    ResponseField[] responseFields() default {};

    Class responseEntity() default Map.class;

    Header[] headers() default {};

    String charset() default Constant.UTF_8;

    Class<? extends HttpHandler>[] handlers() default {};
}
