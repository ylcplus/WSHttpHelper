package org.ws.httphelper.support.pipeline.handler.response;

import org.ws.httphelper.core.pipeline.HandlerOrder;
import org.ws.httphelper.core.pipeline.ResponseHandler;
import org.ws.httphelper.exception.ResponseException;
import org.ws.httphelper.model.RequestContext;
import org.ws.httphelper.model.http.ResponseFuture;

@HandlerOrder(99)
public class DefaultBodyResponseHandler implements ResponseHandler {
    @Override
    public boolean handler(RequestContext requestContext) throws ResponseException {
        ResponseFuture responseFuture = requestContext.getResponseFuture();
        if(responseFuture.isSuccess()){
            responseFuture.setOutput(responseFuture.getResponse().getBody());
        }
        return true;
    }

}
