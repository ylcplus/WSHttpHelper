package org.ws.httphelper.example.rest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.ws.httphelper.HttpHelperSampleApplication;
import org.ws.httphelper.model.UserEntity;
import org.ws.httphelper.model.http.ResponseFuture;

import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SpringBootTest(
        classes = HttpHelperSampleApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT
)
@RunWith(SpringRunner.class)
@Slf4j
public class TestMethodApiTest {

    @Autowired
    private TestMethodApi testMethodApi;

    @Test
    public void testGet(){
        UserEntity userEntity = new UserEntity();
        userEntity.setAge(1);
        userEntity.setName("HH");
        userEntity.setSix("BB");
        ResponseFuture responseFuture = testMethodApi.getSync(userEntity);
        Assert.assertTrue(responseFuture.isSuccess());
        log.info(JSON.toJSONString(responseFuture, SerializerFeature.PrettyFormat));

        Map<String, String> mapSync = testMethodApi.getMapSync(userEntity, Map.class);
        log.info(JSON.toJSONString(mapSync, SerializerFeature.PrettyFormat));

        testMethodApi.getAsync(userEntity, future->{
            Assert.assertTrue(future.isSuccess());
            log.info(JSON.toJSONString(future, SerializerFeature.PrettyFormat));
        });
    }

    @Test
    public void testStressTestingGet() throws InterruptedException {
        int count = 500_000;
        int threadNum = 100;
        log.info("同步Get压力测试,并行线程:{},请求数量:{}",threadNum,count);
        ExecutorService executorService = Executors.newFixedThreadPool(threadNum);
        CountDownLatch countDownLatch = new CountDownLatch(count);
        long start = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
            executorService.submit(()->{
                try {
                    UserEntity userEntity = new UserEntity();
                    userEntity.setAge(1);
                    userEntity.setName("HH");
                    userEntity.setSix("BB");
                    ResponseFuture future = testMethodApi.getSync(userEntity);
                    Assert.assertTrue(future.isSuccess());
                }
                finally {
                    countDownLatch.countDown();
                }

            });
        }
        countDownLatch.await();

        long end = System.currentTimeMillis();
        long time = end-start;
        log.info("耗时:{}ms,并发:{}",time,count/(time/1000));
    }

    @Test
    public void testStressTestingGetAsync() throws InterruptedException {
        int count = 500_000;
        log.info("异步Get压力测试,并行线程:{},请求数量:{}",100,count);
        long start = System.currentTimeMillis();
        CountDownLatch countDownLatch = new CountDownLatch(count);
        for (int i = 0; i < count; i++) {
            UserEntity userEntity = new UserEntity();
            userEntity.setAge(1);
            userEntity.setName("HH");
            userEntity.setSix("BB");
            testMethodApi.getAsync(userEntity, future -> {
                try {
                    Assert.assertTrue(future.isSuccess());
                } finally {
                    countDownLatch.countDown();
                }
            });
        }
        countDownLatch.await();
        long end = System.currentTimeMillis();
        long time = end-start;
        log.info("耗时:{}ms,并发:{}",time,count/(time/1000));
    }
}
