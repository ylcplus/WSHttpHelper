package org.ws.httphelper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.ws.httphelper.model.http.ResponseFuture;
import org.ws.httphelper.example.annotation.TopBaidu;
import org.ws.httphelper.example.annotation.TopNews;
import org.ws.httphelper.example.openapi.BaiduMapSearchApi;
import org.ws.httphelper.example.openapi.SearchRegionParam;
import org.ws.httphelper.example.openapi.SearchRegionResult;
import org.ws.httphelper.example.builder.BuilderSampleService;

import java.util.HashMap;
import java.util.Map;

@Api
@RestController
public class SampleController {

    @Autowired
    private BuilderSampleService builderSampleService;

    @Autowired
    private TopBaidu topBaidu;

    @Autowired
    private HttpHelper httpHelper;

    @Autowired
    private BaiduMapSearchApi baiduMapSearchApi;

    @ApiOperation("Builder示例:下载输入的url的响应html中全部图片")
    @GetMapping("/builderSearchImg")
    public String builderSearchImg(String url){
        builderSampleService.searchAndDownload(url);
        return "OK";
    }

    @ApiOperation("Annotation示例:获取百度实时热点")
    @GetMapping("/topBaidu")
    public TopNews topBaiduRequest(){
        return topBaidu.getTopNews();
    }

    @ApiOperation("Annotation示例:输入URL获取百度实时热点")
    @GetMapping("/topBaidu/url")
    public TopNews topBaiduRequestByUrl(String url){
        return topBaidu.getTopNewsByUrl(url);
    }

    @ApiOperation("Properties示例:请求输入URL")
    @GetMapping("/httpHelper/url")
    public ResponseFuture httpHelper(String url){
        return httpHelper.request(url);
    }

    @GetMapping("/searchRegion/entity")
    public SearchRegionResult searchRegion(String query,String region,String ak,String sk){
        SearchRegionParam param = new SearchRegionParam();
        param.setQuery(query);
        param.setRegion(region);
        param.setAk(ak);
        param.setSk(sk);
        return baiduMapSearchApi.searchRegionByEntity("/search",param);
    }

    @GetMapping("/searchRegion/map")
    public SearchRegionResult searchRegionByMap(String query,String region,String ak,String sk){
        Map<String,String> param = new HashMap<>(8);
        param.put("query",query);
        param.put("region",region);
        param.put("ak",ak);
        param.put("sk",sk);

        return baiduMapSearchApi.searchRegionByMap("/search",param,SearchRegionResult.class);
    }
}
