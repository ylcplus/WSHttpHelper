package org.ws.httphelper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.annotation.Order;
import org.ws.httphelper.spring.EnableHttpHelper;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Order
@Slf4j
@EnableHttpHelper
@EnableSwagger2
@SpringBootApplication
public class HttpHelperSampleApplication {

    public static void main(String[] args) {
        log.info("HttpHelperSample Application start ");
        SpringApplication.run(HttpHelperSampleApplication.class, args);
    }

}
